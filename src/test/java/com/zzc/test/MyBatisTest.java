package com.zzc.test;

import com.mysql.cj.Session;
import com.zzc.domain.User;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

public class MyBatisTest {
    @Test
    public void test1()throws IOException{
        //获取核心配置文件
        InputStream resourceAsStream=Resources.getResourceAsStream("mysqlMapconfig.xml");
        //获取session工厂对象
        SqlSessionFactory sqlSessionFactory= new SqlSessionFactoryBuilder().build(resourceAsStream);
        //获取session会话对象
        SqlSession sqlSession =sqlSessionFactory.openSession();
        //执行操作 参数：id | username | jobs   | phone
        List<User>userList =sqlSession.selectList("userMapper.findAll");
        //打印数据
        System.out.println(userList);
        //释放流
        sqlSession.close();




    }
}
